#!/bin/bash

#generates split m3u8 file for every subtitles
write_subtitles_m3u8 () {
	subtitle_m3u8=$1
	duration=$2
	subtitle_file=$3

	if [ ! -f $subtitle_m3u8 ];then
        echo "#EXTM3U" > $subtitle_m3u8
        echo "#EXT-X-TARGETDURATION:$duration" >> $subtitle_m3u8
        echo "#EXT-X-VERSION:3" >> $subtitle_m3u8
        echo "#EXT-X-MEDIA-SEQUENCE:1" >> $subtitle_m3u8
        echo "#EXT-X-PLAYLIST-TYPE:VOD" >> $subtitle_m3u8
        echo "#EXTINF:$duration," >> $subtitle_m3u8
        echo "$subtitle_file" >> $subtitle_m3u8
        echo "#EXT-X-ENDLIST" >> $subtitle_m3u8
	fi
}

#generates master m3u8 file for video
write_hd_master_m3u8 () {
	$hd_master_m3u8=$1

	if [ ! -f  $hd_master_m3u8 ];then
		echo "#EXTM3U" > $hd_master_m3u8

		while read p; do
			lang=`echo $p | awk '{ printf "%s\n", substr($1,1,2) }' | tr '[A-Z]' '[a-z]'`
		  	echo "#EXT-X-MEDIA:TYPE=SUBTITLES,GROUP-ID=\"subs\",NAME=\"$p\",DEFAULT=NO,AUTOSELECT=YES,FORCED=NO,LANGUAGE=\"$lang\",URI=\"subtitle_$p.m3u8\"" >> $hd_master_m3u8
		done <sub.txt

		rm sub.txt

		echo "#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=1144430,SUBTITLES=\"subs\"" >> $hd_master_m3u8
		echo "video/index.m3u8" >> $hd_master_m3u8
	fi
}

# check if the video folder is existing
dir="video"

#if video folder is not existing create a video folder
if [ ! -e $dir ];then
    mkdir $dir
fi

#segmentize the video file with 10 seconds of chunk files and stores it in the video folder
#set video codec as lib264
#video profiling as main (which supports in all browsers)
#preset as ultrafast to segmentize in shorter time
#video and audio bitrate has been set
#insert the keyframe after every 10 seconds
#copy the video and audio as the original file
#segmentize in 10 seconds chunk file
ffmpeg -i brooklyn.mkv -vcodec libx264 -vprofile main -preset ultrafast -b:v 1000k -maxrate 1000k -bufsize 1000k -threads 0 -b:a 64k -force_key_frames expr:"gte(t,n_forced*10)" -c copy -map 0 -vbsf h264_mp4toannexb -f segment -segment_time 00:10 -segment_list "video/index.m3u8" -segment_format mpegts "video/sv_%05d.ts"

#get the total duration of video in seconds
duration=`ffprobe -i brooklyn.mkv -show_entries format=duration -v quiet -of csv="p=0"`

#remove video file after segmentation
rm brooklyn.mkv

#create a file
touch sub.txt

#convert the subtitles from srt to vtt files, generates m3u8 file for subtitle and then delete the existing srt file
for entry in subtitles/*
do
	file_wo_path=$(basename -- "$entry")
	extension="${file_wo_path##*.}"
	filename="${entry%.*}"
	filename_wo_path="${file_wo_path%.*}"
	echo $filename_wo_path >> sub.txt
	subtitle_m3u8="subtitle_${filename_wo_path}.m3u8"
	subtitle_file="${filename}.vtt"
	write_subtitles_m3u8 $subtitle_m3u8 $duration $subtitle_file
	if [ $extension != "vtt" ];then
		sub_charenc=`file $entry | sed -r "s/^subtitles\/$file_wo_path: (.*)$/\1/" | awk '{print $1;}'`
		if [ $sub_charenc = "ISO-8859" ];then
			ffmpeg -sub_charenc ISO-8859-1 -i $entry ${filename}.vtt
		else
			ffmpeg -sub_charenc $sub_charenc -i $entry ${filename}.vtt
		fi
		rm $entry
	fi
done

#writes the hd_master_m3u8 file
hd_master_m3u8="hd_master.m3u8"
write_hd_master_m3u8 $hd_master_m3u8

